//
//  Utility.h
//  Video Cropper
//
//  Created by UTHABO on 14/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface Utility : NSObject
+ (void)addRightBorderOfView:(UIView *)view WithColor:(UIColor *)color andWidth:(CGFloat) borderWidth;
+ (void)addBottomBorderOfView:(UIView *)view WithColor:(UIColor *)color andWidth:(CGFloat) borderWidth;
+ (void)addTopBorderOfView:(UIView *)view WithColor:(UIColor *)color andWidth:(CGFloat) borderWidth;
+ (void)addLeftBorderOfView:(UIView *)view WithColor:(UIColor *)color andWidth:(CGFloat) borderWidth;
+ (void)showLoading:(UIView*)view;
+ (void)hideLoading:(UIView*)view;
@end

NS_ASSUME_NONNULL_END
