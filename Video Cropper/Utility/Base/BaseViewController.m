//
//  BaseViewController.m
//  Video Cropper
//
//  Created by UTHABO on 12/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "BaseViewController.h"
#import "UIColor+AppColor.h"
#import "UIColor+HexString.h"
#import "Utility.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.leftItemsSupplementBackButton = true;
    self.view.backgroundColor = [UIColor DefaultBackgroundColor];
//    [Utility addTopBorderOfView:self.view WithColor:[UIColor NavigationBarColor] andWidth:1.5];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
