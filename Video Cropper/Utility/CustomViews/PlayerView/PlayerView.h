//
//  PlayerView.h
//  Video Cropper
//
//  Created by UTHABO on 16/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class AVPlayer;

@interface PlayerView : UIView
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) CALayer *playerLayer;
@end

NS_ASSUME_NONNULL_END
