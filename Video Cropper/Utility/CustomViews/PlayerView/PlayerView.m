//
//  PlayerView.m
//  Video Cropper
//
//  Created by UTHABO on 16/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "PlayerView.h"
@import AVFoundation;

@implementation PlayerView

+ (Class)layerClass {
    return [AVPlayerLayer class];
}

- (AVPlayer *)player {
    return [(AVPlayerLayer *)[self layer] player];
}

- (void)setPlayer:(AVPlayer *)player {
    ((AVPlayerLayer *)[self layer]).videoGravity = AVLayerVideoGravityResizeAspect;
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}

- (void)layoutSublayersOfLayer:(CALayer *)layer {
    [super layoutSublayersOfLayer:layer];
    self.playerLayer.frame = self.bounds;
}

@end
