//
//  CommonNavigationController.m
//  Video Cropper
//
//  Created by UTHABO on 12/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "CommonNavigationController.h"

@interface CommonNavigationController () <UINavigationControllerDelegate>

@end

@implementation CommonNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationBar setTranslucent:false];
    self.delegate = self;
    
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.interactivePopGestureRecognizer.enabled = NO;
    }
}

#pragma mark - Navigation
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    viewController.navigationItem.backBarButtonItem = back;
}

@end
