//
//  UIColor+AppColor.h
//  Video Cropper
//
//  Created by UTHABO on 12/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (AppColor)
+ (UIColor *)NavigationBarTint;
+ (UIColor *)NavigationBarColor;
+ (UIColor *)DefaultBackgroundColor;
+ (UIColor *)TaskBackGroundColor;
+ (UIColor *)TaskBorderColor;
@end

NS_ASSUME_NONNULL_END
