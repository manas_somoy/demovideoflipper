//
//  Constants.h
//  Video Cropper
//
//  Created by UTHABO on 12/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

extern NSString * const NavBarTint;
extern NSString * const NavBarBlack;
extern NSString * const DefaultBackGround;
extern NSString * const TaskBackGround;
extern NSString * const TaskBorder;
#endif /* Constants_h */
