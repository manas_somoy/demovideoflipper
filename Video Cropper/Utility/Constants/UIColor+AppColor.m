//
//  UIColor+AppColor.m
//  Video Cropper
//
//  Created by UTHABO on 12/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "UIColor+AppColor.h"
#import "UIColor+HexString.h"
#import "Constants.h"

@implementation UIColor (AppColor)

+ (UIColor *)NavigationBarTint {
    return [UIColor colorWithHexString:NavBarTint];
}

+ (UIColor *)NavigationBarColor {
    return [UIColor colorWithHexString:NavBarBlack];
}

+ (UIColor *)DefaultBackgroundColor {
    return [UIColor colorWithHexString:DefaultBackGround];
}

+ (UIColor *)TaskBackGroundColor {
    return [UIColor colorWithHexString:TaskBackGround];
}

+ (UIColor *)TaskBorderColor {
    return [UIColor colorWithHexString:TaskBorder];
}

@end
