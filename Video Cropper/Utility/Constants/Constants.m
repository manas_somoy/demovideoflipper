//
//  Constants.m
//  Video Cropper
//
//  Created by UTHABO on 12/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

NSString * const NavBarTint = @"CCCCCC";
NSString * const NavBarBlack = @"1B1B1B";
NSString * const DefaultBackGround = @"1E1E1E";
NSString * const TaskBackGround = @"2B2B2B";
NSString * const TaskBorder = @"575757";
