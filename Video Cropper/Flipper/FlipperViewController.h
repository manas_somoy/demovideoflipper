//
//  FlipperViewController.h
//  Video Cropper
//
//  Created by UTHABO on 14/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "BaseViewController.h"
#import <PhotosUI/PhotosUI.h>
#import "PlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FlipperViewExportCompletionDelegate <NSObject>
@optional
- (void)recieveExportURL:(NSURL *)exportUrl;
@end

@interface FlipperViewController : BaseViewController
@property (nonatomic,strong) id<FlipperViewExportCompletionDelegate>delegate;

@property (weak, nonatomic) IBOutlet UICollectionView *flipTaskCollectionView;
@property (weak, nonatomic) IBOutlet PlayerView *playerView;
@property (weak, nonatomic) IBOutlet UIImageView *playImageView;
@property (weak, nonatomic) IBOutlet UIImageView *sliderPlayImageView;
@property (weak, nonatomic) IBOutlet UIView *scrubberThumbView;
@property (weak, nonatomic) IBOutlet UIView *scrubberContainerView;
@property (weak, nonatomic) IBOutlet UICollectionView *scrubberImagesCollectioniew;
@property (nonatomic, strong) AVURLAsset* videoAsset;
@end

NS_ASSUME_NONNULL_END
