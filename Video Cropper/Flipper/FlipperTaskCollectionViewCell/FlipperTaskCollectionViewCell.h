//
//  FlipperTaskCollectionViewCell.h
//  Video Cropper
//
//  Created by UTHABO on 14/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FlipperTaskCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *taskTitleLabel;

@end

NS_ASSUME_NONNULL_END
