//
//  FlipperTaskCollectionViewCell.m
//  Video Cropper
//
//  Created by UTHABO on 14/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "FlipperTaskCollectionViewCell.h"
#import "UIColor+AppColor.h"
#import "UIColor+HexString.h"

@implementation FlipperTaskCollectionViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    
    _iconImageView.contentMode = UIViewContentModeScaleAspectFit;
    _iconImageView.backgroundColor = [UIColor TaskBackGroundColor];
    _iconImageView.layer.borderWidth = 1;
    _iconImageView.layer.borderColor = [UIColor TaskBorderColor].CGColor;
    
    _taskTitleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    _taskTitleLabel.textColor = UIColor.whiteColor;
    _taskTitleLabel.numberOfLines = 2;
    _taskTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        _iconImageView.backgroundColor = [UIColor NavigationBarColor];
    } else {
        _iconImageView.backgroundColor = [UIColor TaskBackGroundColor];
    }
}
@end
