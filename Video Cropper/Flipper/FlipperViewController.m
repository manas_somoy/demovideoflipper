//
//  FlipperViewController.m
//  Video Cropper
//
//  Created by UTHABO on 14/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "FlipperViewController.h"
#import "FlipperTaskCollectionViewCell.h"
#import <AVFoundation/AVFoundation.h>
#import "Utility.h"
#import "UIColor+AppColor.h"
#import "UIColor+HexString.h"
#import "ScrubCollectionViewCell.h"
#import <SVProgressHUD.h>

#define degreeToRadian(x) (M_PI * x / 180.0)
#define radianToDegree(x) (180.0 * x / M_PI)

@interface FlipperViewController ()<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource> {
    NSArray *taskNames;
    NSArray *taskImageNames;
    CGFloat cellWidth;
    CGFloat cellHeight;
    CGFloat inset;
    CGFloat interItemSpace;
    
    int numberOfImages;
    CGFloat maxXPosition;
    
    CGFloat currentRotation;
    CGFloat currentflipHorizontal;
    CGFloat currentflipVertical;
    
    CGAffineTransform rotateTransform;
    CGAffineTransform scaleTransform;
}

@property (nonatomic) AVPlayer *player;
@property (strong, nonatomic) AVPlayerItem *playerItem;
@property (strong, nonatomic) AVPlayerLayer *playerLayer;
@property (nonatomic) id playerTimeObserver;

@property (strong, nonatomic) UIPanGestureRecognizer *scrubberPan;
@property (nonatomic) CGPoint thumbLastLocation;

@property (strong, nonatomic) NSMutableArray *scrubberImages;

@property (strong, nonatomic) AVAssetExportSession *exportSession;
@end

@implementation FlipperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Flip & Rotate";
    inset = 10;
    interItemSpace = 20;
    cellWidth = 70;
    cellHeight = 140;
    
    currentRotation = 0;
    currentflipHorizontal = 1;
    currentflipVertical = 1;
    
    numberOfImages = 5;
    _scrubberImages = [[NSMutableArray alloc] init];
    _scrubberImagesCollectioniew.delegate = self;
    _scrubberImagesCollectioniew.dataSource = self;
    
    [self setupViews];
    [self initializeTaskArrays];
    [self addNavBarButtons];
    [self addVideoLayer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self generateScrubberImages];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.player pause];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    maxXPosition = CGRectGetMaxX(_scrubberContainerView.bounds);
}

- (void)addNavBarButtons {
    self.navigationItem.leftItemsSupplementBackButton = false;
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    self.navigationItem.leftBarButtonItem = back;
    
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];    self.navigationItem.rightBarButtonItem = done;
}

- (void)setupViews {
    
    self.playImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playTapPressed:)];
    [tap setNumberOfTapsRequired:1];
    [_playerView setUserInteractionEnabled:true];
    [_playerView addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playTapPressed:)];
    [tap2 setNumberOfTapsRequired:1];
    [_sliderPlayImageView setUserInteractionEnabled:true];
    [_sliderPlayImageView addGestureRecognizer:tap2];
    
    self.scrubberPan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(scrubberThumbPanned:)];
    self.scrubberThumbView.userInteractionEnabled = true;
    [self.scrubberThumbView addGestureRecognizer:_scrubberPan];
    
    self.scrubberContainerView.layer.cornerRadius = 5;
//    self.scrubberContainerView.layer.borderColor = UIColor.blackColor.CGColor;
//    self.scrubberContainerView.layer.borderWidth = 0.5;
    
    self.scrubberImagesCollectioniew.layer.cornerRadius = 5;
    self.scrubberImagesCollectioniew.backgroundColor = UIColor.clearColor;
}

- (void)generateScrubberImages {
    
    CGFloat interval = CMTimeGetSeconds(_videoAsset.duration)/numberOfImages;
    NSMutableArray* imageTimes = [[NSMutableArray alloc] init];
    for (int i = 0; i < numberOfImages; i++) {
        [imageTimes addObject:[NSValue valueWithCMTime:CMTimeMakeWithSeconds(i * interval, _videoAsset.duration.timescale)]];
    }
    
    NSMutableArray* scrubImages = [[NSMutableArray alloc] init];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:_videoAsset];
    generator.appliesPreferredTrackTransform=TRUE;
    
    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
        if (result != AVAssetImageGeneratorSucceeded) {
            NSLog(@"couldn't generate thumbnail, error:%@", error);
        }
        
        [scrubImages addObject:[UIImage imageWithCGImage:im]];
        if ((scrubImages.count) == self->numberOfImages) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->_scrubberImages addObjectsFromArray:scrubImages];
                [self->_scrubberImagesCollectioniew reloadData];
            });
        }
    };
    
    CGSize maxSize = CGSizeMake(320, 180);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:imageTimes completionHandler:handler];
    
}


- (void)addVideoLayer {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.playerItem = [AVPlayerItem playerItemWithURL: self.videoAsset.URL];
        self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
        self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
        self.playerLayer.frame = self.playerView.bounds;
        [self.playerView.layer addSublayer:self.playerLayer];
        self.playerView.playerLayer = self.playerLayer;
        
        [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(playerDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
    });
}

//MARK:- Reset Player At End
- (void)playerDidReachEnd:(NSNotification*)notification {
    [self.playerItem seekToTime:kCMTimeZero];
    [self playTapPressed:nil];
    _scrubberThumbView.center = CGPointMake(0, _scrubberThumbView.center.y);
}

//MARK:- Button Action
- (void)backButtonPressed {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void)doneButtonPressed {
//    [Utility showLoading:self.view];
    [self generateExportURLwithcompletion:^(NSURL *exportedVideoFile, NSError *error) {
        NSLog(@"modifiedVideoURL %@",exportedVideoFile);
        if (error) {
            NSString *errorMessage = [NSString stringWithFormat:@"Export Error: %@", [error localizedDescription]];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Try Again!" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            return;
        }
        
        NSLog(@"%@", exportedVideoFile.absoluteString);
        [self.delegate recieveExportURL:exportedVideoFile];
        [self dismissViewControllerAnimated:true completion:nil];
    }];
}

- (void)playTapPressed:(UITapGestureRecognizer *)sender {
    if (self.playImageView.alpha > 0) {
        
        CMTime interval = CMTimeMakeWithSeconds(0.001, NSEC_PER_SEC); // 1 second
        __weak typeof(self) weakSelf = self;
        CGFloat maxX = maxXPosition;
        self.playerTimeObserver = [self.player addPeriodicTimeObserverForInterval:interval queue:NULL usingBlock:^(CMTime time) {
            CGFloat percent = CMTimeGetSeconds(self.playerItem.currentTime) / CMTimeGetSeconds(weakSelf.playerItem.duration);
            CGFloat newX = maxX * percent;
            weakSelf.scrubberThumbView.center = CGPointMake(newX, weakSelf.scrubberThumbView.center.y);
        }];
        
        [self.player play];
        [self.playImageView setAlpha:0];
        [self.sliderPlayImageView setImage:[UIImage imageNamed:@"pause-transparent"]];
    } else {
        if (self.playerTimeObserver != nil) {
            [self.player removeTimeObserver:self.playerTimeObserver];
            self.playerTimeObserver = nil;
        }
        [self.player pause];
        [self.playImageView setAlpha:1];
        [self.sliderPlayImageView setImage:[UIImage imageNamed:@"play-transparent"]];
    }
}

//MARK:- Scrubber Pan
- (void)scrubberThumbPanned:(UIPanGestureRecognizer *)sender {
    
    //Stop the player
    if (self.playerTimeObserver != nil) {
        [self.player removeTimeObserver:self.playerTimeObserver];
        self.playerTimeObserver = nil;
    }
    if (self.playImageView.alpha == 0) {
        [self.player pause];
        [self.playImageView setAlpha:1];
    }
    
    //Move the thumb
    if (sender.state == UIGestureRecognizerStateBegan) {
        _thumbLastLocation = self.scrubberThumbView.center;
    }
    CGPoint point = [sender translationInView:self.scrubberContainerView];
    if (point.x != self.thumbLastLocation.x) {
        CGFloat newX = _thumbLastLocation.x + point.x;
        
        //Thumb Movement
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!(newX < 0) && !(newX > self->maxXPosition)) {
                self.scrubberThumbView.center = CGPointMake(self.thumbLastLocation.x + point.x, self.scrubberThumbView.center.y);
            }
        });
        
        //Set AvPlayer Time
        CGFloat percent = newX / maxXPosition;
        CGFloat passedTime = percent * CMTimeGetSeconds(self.playerItem.duration);
        [self.playerItem seekToTime:CMTimeMakeWithSeconds(passedTime, self.playerItem.currentTime.timescale)];
    }
}

//MARK:- CollectionView Data
- (void)initializeTaskArrays {
    taskNames = [NSArray arrayWithObjects:@"Rotate Right", @"Rotate Left", @"Flip Vertical", @"Flip Horizontal", nil];
    taskImageNames = [NSArray arrayWithObjects:@"rotate_right_VM_Main", @"rotate_left_VM_Main", @"flip_vertical", @"flip_horizontal", nil];
    
    [_flipTaskCollectionView setShowsHorizontalScrollIndicator:false];
    _flipTaskCollectionView.delegate = self;
    _flipTaskCollectionView.dataSource = self;
}

//MARK:- UICollectionView Delegate & Datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == _flipTaskCollectionView) {
        return taskNames.count;
    } else {
        return _scrubberImages.count;
    }
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView == _flipTaskCollectionView) {
        CGFloat horizontalInset = (self.view.bounds.size.width - 3*interItemSpace - 4*cellWidth)/2;
        return UIEdgeInsetsMake(inset, horizontalInset, inset, horizontalInset);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == _flipTaskCollectionView) {
        return interItemSpace;
    }
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == _flipTaskCollectionView) {
        return interItemSpace;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == _flipTaskCollectionView) {
        return CGSizeMake(cellWidth, cellHeight);
    }
    return CGSizeMake(_scrubberImagesCollectioniew.bounds.size.width/numberOfImages, _scrubberImagesCollectioniew.bounds.size.height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == _flipTaskCollectionView) {
        FlipperTaskCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"FlipperTaskCollectionViewCell" forIndexPath: indexPath];
        
        cell.iconImageView.image = [UIImage imageNamed:taskImageNames[indexPath.item]];
        cell.taskTitleLabel.text = taskNames[indexPath.item];
        
        return cell;
    }
    
    ScrubCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ScrubCollectionViewCell" forIndexPath: indexPath];
    
    cell.iconImageView.image = [_scrubberImages objectAtIndex:indexPath.item];
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == _flipTaskCollectionView) {
        return true;
    }
    return false;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:true];
    [self translatePlayerLayer:indexPath];
}

//MARK:- ANimation Player View
- (void)translatePlayerLayer: (NSIndexPath *)indexPath{
    CABasicAnimation *flash = [CABasicAnimation animationWithKeyPath:@"opacity"];
    flash.fromValue = [NSNumber numberWithFloat:0.0];
    flash.toValue = [NSNumber numberWithFloat:1.0];
    flash.duration = 0.60;
    [self.playerLayer addAnimation:flash forKey:@"flashAnimation"];
    
    [CATransaction begin];
    [CATransaction setValue: (id) kCFBooleanTrue forKey: kCATransactionDisableActions];
    if (indexPath.item == 0) {
        if (currentflipVertical == currentflipHorizontal) {
            currentRotation += 90;
            [self rotateVideoPlayerWithDegree:90];
        } else {
            currentRotation += -90;
            [self rotateVideoPlayerWithDegree:-90];
        }
    } else if (indexPath.item == 1) {
        if (currentflipVertical == currentflipHorizontal) {
            currentRotation += -90;
            [self rotateVideoPlayerWithDegree:-90];
        } else {
            currentRotation += 90;
            [self rotateVideoPlayerWithDegree:90];
        }
    } else if (indexPath.item == 2) {
        if ([self checkIfLayerRotateNotVertical]) {
            currentflipVertical = (-1) * currentflipVertical;
            [self flipVertical];
        } else {
            currentflipHorizontal = (-1) * currentflipHorizontal;
            [self flipHorizontal];
        }
    } else if (indexPath.item == 3) {
        if ([self checkIfLayerRotateNotVertical]) {
            currentflipHorizontal = (-1) * currentflipHorizontal;
            [self flipHorizontal];
        } else {
            currentflipVertical = (-1) * currentflipVertical;
            [self flipVertical];
        }
    }
    [CATransaction commit];
}

- (BOOL)checkIfLayerRotateNotVertical {
    CGFloat rotation = fabs(currentRotation);
    return fmodf((rotation/90), 2) == 0;
}

- (void)flipVertical {
    if (CGAffineTransformIsIdentity(self.playerLayer.affineTransform)) {
        self.playerLayer.affineTransform = CGAffineTransformMakeScale(1, -1); //CATransform3DMakeScale(1, -1, 1);
    } else {
        self.playerLayer.affineTransform = CGAffineTransformScale(self.playerLayer.affineTransform, 1, -1); //CATransform3DScale(self.playerLayer.transform, 1, -1, 1);
    }
}

- (void)flipHorizontal {
    if (CGAffineTransformIsIdentity(self.playerLayer.affineTransform)) {
        self.playerLayer.affineTransform = CGAffineTransformMakeScale(-1, 1); //CATransform3DMakeScale(-1, 1, 1);
    } else {
        self.playerLayer.affineTransform = CGAffineTransformScale(self.playerLayer.affineTransform, -1, 1); //CATransform3DScale(self.playerLayer.transform, -1, 1, 1);
    }
}

- (void)rotateVideoPlayerWithDegree:(CGFloat)degree {
    if (CGAffineTransformIsIdentity(self.playerLayer.affineTransform)) {
        self.playerLayer.affineTransform = CGAffineTransformMakeRotation(degreeToRadian(degree)); //CATransform3DMakeRotation(degreeToRadian(degree), 0.0, 0.0, 1.0);
    } else {
        self.playerLayer.affineTransform = CGAffineTransformRotate(self.playerLayer.affineTransform, degreeToRadian(degree)); //CATransform3DRotate(self.playerLayer.transform, degreeToRadian(degree), 0.0, 0.0, 1.0);
    }
}

//MARK:- Export Changed VIdeo
- (void)generateExportURLwithcompletion:(void(^)(NSURL *mergedVideoFile, NSError *error))completion {
    [SVProgressHUD showWithStatus:@"Exporting new video..."];
    
    NSLog(@"Video URL: ================ %@", _videoAsset.URL.absoluteString);
    NSLog(@"Video Duration: =============== %f", CMTimeGetSeconds(_videoAsset.duration));
    AVMutableComposition* composition = [AVMutableComposition composition];
    NSArray *dataSourceArray = [NSArray arrayWithArray: [_videoAsset tracksWithMediaType:AVMediaTypeVideo]];
    NSError *error;
    NSLog(@"array count => %lu",(unsigned long)[dataSourceArray count]);
    CMTimeRange timeRange = CMTimeRangeMake(kCMTimeZero, _videoAsset.duration);
    AVMutableCompositionTrack *videoTrack2 = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:0];
    AVMutableCompositionTrack *firstTrackAudio = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    AVAssetTrack *clipAudioTrack = [[_videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    [firstTrackAudio insertTimeRange:CMTimeRangeMake(kCMTimeZero, _videoAsset.duration) ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
    NSArray *videoAssetTracks2 = [_videoAsset tracksWithMediaType:AVMediaTypeVideo];
    AVAssetTrack *clipVideoTrack = ([videoAssetTracks2 count] > 0 ? [videoAssetTracks2 objectAtIndex:0] : nil);
    [videoTrack2 insertTimeRange:timeRange ofTrack:clipVideoTrack atTime:kCMTimeZero error:&error];
    
    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
    CGSize size = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height);
    
    int32_t currentFrameRate = (int)roundf(clipVideoTrack.nominalFrameRate);
    videoComposition.frameDuration = CMTimeMake(1, currentFrameRate);
    
//    layer instruction
    AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack2];
    
//    CGAffineTransform trO = [self transformBasedOnAsset:[AVAsset assetWithURL:_videoAsset.URL]];
//    videoTrack2.preferredTransform = trO;
    
    CGAffineTransform transform = _videoAsset.preferredTransform;
    
    if ((currentRotation != 0) && !(fmodf(currentRotation, 360) == 0)) {
        if (fmodf(currentRotation, 270) == 0) {
            if (currentRotation < 0) {
                transform = CGAffineTransformTranslate(transform, clipVideoTrack.naturalSize.width/1.78, 0);
            } else {
                transform = CGAffineTransformTranslate(transform, 0, 1.78*clipVideoTrack.naturalSize.height);
            }
            currentRotation = currentRotation < 0 ? -270 : 270;
        } else if (fmodf(currentRotation, 180) == 0) {
            transform = CGAffineTransformTranslate(transform, clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height);
            currentRotation = currentRotation < 0 ? -180 : 180;
        } else if (fmodf(currentRotation, 90) == 0) {
            if (currentRotation < 0) {
                transform = CGAffineTransformTranslate(transform, 0, clipVideoTrack.naturalSize.width);
            } else {
                transform = CGAffineTransformTranslate(transform, clipVideoTrack.naturalSize.height, 0);
            }
            currentRotation = currentRotation < 0 ? -90 : 90;
        }
        
        transform = CGAffineTransformRotate(transform, degreeToRadian(currentRotation));
    }
    
    if (currentflipHorizontal < 1) {
        transform = CGAffineTransformTranslate(transform, clipVideoTrack.naturalSize.width, 0);
    }
    if (currentflipVertical < 1) {
        transform = CGAffineTransformTranslate(transform, 0, clipVideoTrack.naturalSize.height);
    }
    if (currentflipVertical == -1 || currentflipHorizontal == -1) {
        transform = CGAffineTransformScale(transform, currentflipHorizontal, currentflipVertical);
    }
    
    [transformer setTransform:transform atTime:kCMTimeZero];
    
    AVMutableVideoCompositionInstruction* instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, _videoAsset.duration);
    instruction.layerInstructions = @[transformer];
    videoComposition.instructions = @[instruction];
    if (![self checkIfLayerRotateNotVertical]) {
        size = CGSizeMake(size.height, size.width);
    }
    videoComposition.renderSize = size;
    
    //Export Preset
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:_videoAsset];
    NSString* preset = AVAssetExportPresetPassthrough;
    if ([compatiblePresets containsObject:AVAssetExportPresetHighestQuality]) {
        preset = AVAssetExportPresetHighestQuality;
    }
    
    //Export Session
    self.exportSession = [[AVAssetExportSession alloc] initWithAsset:composition presetName:preset];
    AVFileType type;
    if ([[self.exportSession supportedFileTypes] containsObject:AVFileTypeMPEG4]) {
        type = AVFileTypeMPEG4;
    } else {
        type = AVFileTypeQuickTimeMovie;
    }
    NSString *fileName = [FlipperViewController generateFileName];
    NSString *filePath = [FlipperViewController documentsPathWithFilePath:fileName];
    self.exportSession.outputURL = [NSURL fileURLWithPath:filePath];
    self.exportSession.outputFileType = type;
    self.exportSession.shouldOptimizeForNetworkUse = YES;
    self.exportSession.videoComposition = videoComposition;
    
    
    NSLog(@"Composition Duration: %ld seconds", lround(CMTimeGetSeconds(composition.duration)));
    NSLog(@"Composition Framerate: %d fps", currentFrameRate);
    
    void(^exportCompletion)(void) = ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) completion(self.exportSession.outputURL, self.exportSession.error);
        });
    };
    
    [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        switch (self.exportSession.status) {
            case AVAssetExportSessionStatusFailed:{
                NSLog(@"Failed:%@", self.exportSession.error);
                exportCompletion();
                break;
            }
            case AVAssetExportSessionStatusCancelled:{
                NSLog(@"Canceled:%@", self.exportSession.error);
                exportCompletion();
                break;
            }
            case AVAssetExportSessionStatusCompleted: {
                NSLog(@"Successfully merged video files into: %@", fileName);
                exportCompletion();
                break;
            }
            case AVAssetExportSessionStatusUnknown: {
                NSLog(@"Export Status: Unknown");
            }
            case AVAssetExportSessionStatusExporting : {
                NSLog(@"Export Status: Exporting");
            }
            case AVAssetExportSessionStatusWaiting: {
                NSLog(@"Export Status: Wating");
            }
            default:
                break;
        };
    }];
}

- (UIInterfaceOrientation)orientationForTrack:(AVAsset *)asset {
    UIInterfaceOrientation orientation = UIInterfaceOrientationPortrait;
    NSArray *tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    
    if([tracks count] > 0) {
        AVAssetTrack *videoTrack = [tracks objectAtIndex:0];
        CGAffineTransform t = videoTrack.preferredTransform;
        
        // Portrait
        if(t.a == 0 && t.b == 1.0 && t.c == -1.0 && t.d == 0) {
            orientation = UIInterfaceOrientationPortrait;
        }
        // PortraitUpsideDown
        if(t.a == 0 && t.b == -1.0 && t.c == 1.0 && t.d == 0) {
            orientation = UIInterfaceOrientationPortraitUpsideDown;
        }
        // LandscapeRight
        if(t.a == 1.0 && t.b == 0 && t.c == 0 && t.d == 1.0) {
            orientation = UIInterfaceOrientationLandscapeRight;
        }
        // LandscapeLeft
        if(t.a == -1.0 && t.b == 0 && t.c == 0 && t.d == -1.0) {
            orientation = UIInterfaceOrientationLandscapeLeft;
        }
    }
    return orientation;
}

- (CGAffineTransform)transformBasedOnAsset:(AVAsset *)asset {
    UIInterfaceOrientation orientation = [self orientationForTrack:asset];
    AVAssetTrack *assetTrack = [asset tracksWithMediaType:AVMediaTypeVideo][0];
    CGSize naturalSize = assetTrack.naturalSize;
    CGAffineTransform finalTranform;
    switch (orientation) {
        case UIInterfaceOrientationLandscapeLeft:
            finalTranform = CGAffineTransformMake(-1, 0, 0, -1, naturalSize.width, naturalSize.height);
            break;
        case UIInterfaceOrientationLandscapeRight:
            finalTranform = CGAffineTransformMake(1, 0, 0, 1, 0, 0);
            break;
        case UIInterfaceOrientationPortrait:
            finalTranform = CGAffineTransformMake(0, 1, -1, 0, naturalSize.height, 0);
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            finalTranform = CGAffineTransformMake(0, -1, 1, 0, 0, naturalSize.width);
            break;
        default:
            break;
    }
    return finalTranform;
}

+ (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

+ (NSString *)documentsPathWithFilePath:(NSString *)filePath {
    return [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:filePath];
}

+ (NSString *)generateFileName {
    return [NSString stringWithFormat:@"video-%@.mp4", [[NSProcessInfo processInfo] globallyUniqueString]];
}

@end
