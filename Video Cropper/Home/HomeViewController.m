//
//  HomeViewController.m
//  Video Cropper
//
//  Created by UTHABO on 12/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "HomeViewController.h"
#import <PhotosUI/PhotosUI.h>
#import "VideoCollectionViewCell/VideoCollectionViewCell.h"
#import "EditorViewController.h"

@interface HomeViewController () <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource> {
    CGFloat scaleFactor;
    CGFloat cellWidth;
    CGFloat cellHeight;
    CGFloat inset;
    CGFloat interItemSpace;
}

//@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
//@property (nonatomic, strong) NSURL *videoURL;
//@property (nonatomic, strong) MPMoviePlayerController *mpVideoPlayer;
@property (nonatomic, strong) NSMutableArray *videoURLArray;
@property (nonatomic, strong) NSMutableArray *assetItems;
//@property (nonatomic, strong) NSMutableDictionary *dic;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"All Videos";
    self.view.backgroundColor = UIColor.blackColor;
    scaleFactor = 4.0;
    inset = 5;
    interItemSpace = 3;
    cellWidth = (UIScreen.mainScreen.bounds.size.width - (2 * inset) - (3 * interItemSpace)) / scaleFactor;
    cellHeight = cellWidth;
    
    _videoCollectionView.delegate = self;
    _videoCollectionView.dataSource = self;
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if ((status == PHAuthorizationStatusNotDetermined) || (status == PHAuthorizationStatusRestricted) || (status == PHAuthorizationStatusDenied)) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                [self getAllVideos];
            } else {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Permission Denied!" message:@"Video Crop must have access to the photo library. Please provide library access permission from settings." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized) {
        [self getAllVideos];
    }
}

//MARK:- Setup CollectionView
- (void)setupCollectionView {
    
}

//MARK:- Get All Videos From Library
- (void)getAllVideos {
    PHFetchOptions *option = [[PHFetchOptions alloc] init];
    option.sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:@"creationDate" ascending:false]];
    
    PHFetchResult *result = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:option];
    
    _assetItems = [NSMutableArray new];
    [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[PHAsset class]]) {
            PHAsset *item = (PHAsset *)obj;
            [self.assetItems addObject:item];
            printf("%ld", (long)item.mediaType);
        }
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_videoCollectionView reloadData];
    });
}

//MARK:- UICollectionView Delegate & Datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _assetItems.count;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(inset, inset, 2*inset, inset);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return interItemSpace;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return interItemSpace + 2;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(cellWidth, cellHeight);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    VideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"VideoCollectionViewCell" forIndexPath: indexPath];
    
    PHAsset *asset = [_assetItems objectAtIndex:indexPath.item];
    cell.videoLengthLabel.text = [self stringFromTimeInterval:asset.duration];
    [PHImageManager.defaultManager requestImageForAsset:asset targetSize:CGSizeMake(cellHeight*3, cellHeight*3) contentMode:PHImageContentModeDefault options:nil resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        cell.thumbnailImageView.image = result;
    }];
    
    return cell;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    NSString* hourString = hours > 0 ? [NSString stringWithFormat:@"%02ld:", (long)hours] : @"";
    return [NSString stringWithFormat:@"%@%02ld:%02ld", hourString, (long)minutes, (long)seconds];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EditorViewController *editVC = [main instantiateViewControllerWithIdentifier:@"EditorViewController"];
    PHAsset *asset = [_assetItems objectAtIndex:indexPath.item];
    PHVideoRequestOptions *option = [PHVideoRequestOptions new];
    option.deliveryMode = PHImageRequestOptionsDeliveryModeOpportunistic;
    [PHImageManager.defaultManager requestAVAssetForVideo:asset options:option resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
        
        editVC.asset = (AVURLAsset*)asset;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController pushViewController:editVC animated:true];
        });
    }];
}



@end
