//
//  HomeViewController.h
//  Video Cropper
//
//  Created by UTHABO on 12/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UICollectionView *videoCollectionView;
@end

NS_ASSUME_NONNULL_END
