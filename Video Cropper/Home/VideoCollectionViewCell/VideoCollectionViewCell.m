//
//  VideoCollectionViewCell.m
//  Video Cropper
//
//  Created by UTHABO on 12/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "VideoCollectionViewCell.h"

@implementation VideoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
//    self.layer.cornerRadius = 1;
//    self.layer.borderWidth = 1;
    self.layer.borderColor = UIColor.whiteColor.CGColor;
    self.videoLengthLabel.layer.borderWidth = 1;
    self.videoLengthLabel.layer.borderColor = UIColor.blackColor.CGColor;
    self.videoLengthLabel.layer.cornerRadius = 4;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        self.layer.borderWidth = 5;
    } else {
        self.layer.borderWidth = 0;
    }
}
@end
