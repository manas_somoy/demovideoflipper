//
//  ScrubCollectionViewCell.m
//  Video Cropper
//
//  Created by UTHABO on 20/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "ScrubCollectionViewCell.h"

@implementation ScrubCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
}
@end
