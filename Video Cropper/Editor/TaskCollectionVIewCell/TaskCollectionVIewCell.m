//
//  TaskCollectionVIewCell.m
//  Video Cropper
//
//  Created by UTHABO on 14/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "TaskCollectionVIewCell.h"
#import "UIColor+AppColor.h"
#import "UIColor+HexString.h"

@implementation TaskCollectionVIewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _iconImageView.contentMode = UIViewContentModeScaleAspectFit;
    _iconImageView.backgroundColor = [UIColor TaskBackGroundColor];
    _iconImageView.layer.borderWidth = 1;
    _iconImageView.layer.borderColor = [UIColor TaskBorderColor].CGColor;
    
    _taskTitleLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightMedium];
    _taskTitleLabel.textColor = UIColor.whiteColor;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        _iconImageView.backgroundColor = [UIColor NavigationBarColor];
    } else {
        _iconImageView.backgroundColor = [UIColor TaskBackGroundColor];
    }
}
@end
