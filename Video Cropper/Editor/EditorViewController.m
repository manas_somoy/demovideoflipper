//
//  EditorViewController.m
//  Video Cropper
//
//  Created by UTHABO on 14/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "EditorViewController.h"
#import "TaskCollectionVIewCell/TaskCollectionVIewCell.h"
#import "CommonNavigationController.h"
#import "FlipperViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "Utility.h"
#import "UIColor+AppColor.h"
#import "UIColor+HexString.h"
#import "ScrubCollectionViewCell.h"

@interface EditorViewController ()<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, FlipperViewExportCompletionDelegate> {
    NSArray *taskNames;
    NSArray *taskImageNames;
    CGFloat cellWidth;
    CGFloat cellHeight;
    CGFloat inset;
    CGFloat interItemSpace;
    int numberOfImages;
    CGFloat maxXPosition;
}

@property (weak, nonatomic) IBOutlet UIImageView *playImageView;
@property (weak, nonatomic) IBOutlet UIButton *volumeButton;
@property (weak, nonatomic) IBOutlet UIView *trimmerView;

@property (nonatomic) AVPlayer *player;
@property (strong, nonatomic) AVPlayerItem *playerItem;
@property (strong, nonatomic) AVPlayerLayer *playerLayer;

@property (nonatomic) id playerTimeObserver;
@property (strong, nonatomic) UIPanGestureRecognizer *scrubberPan;
@property (nonatomic) CGPoint thumbLastLocation;

@property (strong, nonatomic) NSMutableArray *scrubberImages;
@end

@implementation EditorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Edit Video";
    inset = 20;
    interItemSpace = 20;
    cellWidth = 65;
    cellHeight = 100;
    numberOfImages = 5;
    _scrubberImages = [[NSMutableArray alloc] init];
    _scrubberImagesCollectioniew.delegate = self;
    _scrubberImagesCollectioniew.dataSource = self;
    
    [self avPlayerSetup];
    [self setupViews];
    [self initializeTaskArrays];
    [self addNavBarButtons];
    [self generateScrubberImages];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.player pause];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    maxXPosition = CGRectGetMaxX(_scrubberContainerView.bounds);
}

- (void)setupViews {
    
    self.playImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.volumeButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playTapPressed:)];
    [tap setNumberOfTapsRequired:1];
    [_playerView setUserInteractionEnabled:true];
    [_playerView addGestureRecognizer:tap];
    
    self.scrubberPan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(scrubberThumbPanned:)];
    self.scrubberThumbView.userInteractionEnabled = true;
    [self.scrubberThumbView addGestureRecognizer:_scrubberPan];
    
    self.scrubberContainerView.layer.cornerRadius = 10;
    
    self.scrubberImagesCollectioniew.layer.cornerRadius = 10;
    self.scrubberImagesCollectioniew.backgroundColor = UIColor.clearColor;
    self.scrubberImagesCollectioniew.layer.borderColor = [UIColor NavigationBarTint].CGColor;
    self.scrubberImagesCollectioniew.layer.borderWidth = 1;
}

- (void)generateScrubberImages {
    
    CGFloat interval = CMTimeGetSeconds(_asset.duration)/numberOfImages;
    NSMutableArray* imageTimes = [[NSMutableArray alloc] init];
    for (int i = 0; i < numberOfImages; i++) {
        [imageTimes addObject:[NSValue valueWithCMTime:CMTimeMakeWithSeconds(i * interval, _asset.duration.timescale)]];
    }
    
    NSMutableArray* scrubImages = [[NSMutableArray alloc] init];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:_asset];
    generator.appliesPreferredTrackTransform=TRUE;
    
    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
        if (result != AVAssetImageGeneratorSucceeded) {
            NSLog(@"couldn't generate thumbnail, error:%@", error);
        }
        
        [scrubImages addObject:[UIImage imageWithCGImage:im]];
        if ((scrubImages.count) == self->numberOfImages) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self->_scrubberImages.count > 0) {
                    [self->_scrubberImages removeAllObjects];
                }
                [self->_scrubberImages addObjectsFromArray:scrubImages];
                [self->_scrubberImagesCollectioniew reloadData];
            });
        }
    };
    
    CGSize maxSize = CGSizeMake(320, 180);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:imageTimes completionHandler:handler];
    
}

- (void)avPlayerSetup {
    
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.playerItem = [AVPlayerItem playerItemWithURL: self.asset.URL];
            self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
            self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
            self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
            self.playerLayer.frame = self.playerView.bounds;
            [self.playerView.layer addSublayer:self.playerLayer];
            self.playerView.playerLayer = self.playerLayer;
            
            [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(playerDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
        });
}

//MARK:- Reset Player At End
- (void)playerDidReachEnd:(NSNotification*)notification {
    [self.playerItem seekToTime:kCMTimeZero];
    [self playTapPressed:nil];
    _scrubberThumbView.center = CGPointMake(0, _scrubberThumbView.center.y);
}

//MARK:- CollectionView Data
- (void)initializeTaskArrays {
    taskNames = [NSArray arrayWithObjects:@"Crop", @"Trim & Cut", @"Flip/Rotate", @"Filter", @"Add Music", @"Volume", @"Tutorial", nil];
    taskImageNames = [NSArray arrayWithObjects:@"crop", @"trim&cut", @"flip&rotate", @"filter", @"add-music", @"volume", @"tutorial", nil];
    
    [_taskCollectionView setShowsHorizontalScrollIndicator:false];
    _taskCollectionView.delegate = self;
    _taskCollectionView.dataSource = self;
}

- (void)addNavBarButtons {
    UIBarButtonItem *reload = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:nil action:nil];
    self.navigationItem.leftBarButtonItem = reload;
    
    UIBarButtonItem *purchase = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"purchase"] style:UIBarButtonItemStylePlain target:nil action:nil];
    UIBarButtonItem *share = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareButtonPressed)];
    self.navigationItem.rightBarButtonItems = @[share, purchase];
}

//MARK:- Flipper View Completion Delegate
- (void)recieveExportURL:(NSURL *)exportUrl {
    if (self.playerTimeObserver != nil) {
        [self.player removeTimeObserver:self.playerTimeObserver];
        self.playerTimeObserver = nil;
    }
    _asset = [AVURLAsset assetWithURL:exportUrl];
    _playerView.layer.sublayers = nil;
    [self avPlayerSetup];
    [self generateScrubberImages];
    
//    UISaveVideoAtPathToSavedPhotosAlbum([exportUrl relativePath], self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
}

//MARK:- Save Completion
- (void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo {
    
    if (error) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Video Saving Failed" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Video Saved" message:@"Saved To Photo Album" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Great!" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

//MARK:- Button Action
- (void)shareButtonPressed {
    UISaveVideoAtPathToSavedPhotosAlbum([_asset.URL relativePath], self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
}

- (void)playTapPressed:(UITapGestureRecognizer *)sender {
    if (self.playImageView.alpha > 0) {
        
        CMTime interval = CMTimeMakeWithSeconds(0.001, NSEC_PER_SEC); // 1 second
        __weak typeof(self) weakSelf = self;
        CGFloat maxX = maxXPosition;
        self.playerTimeObserver = [self.player addPeriodicTimeObserverForInterval:interval queue:NULL usingBlock:^(CMTime time) {
            CGFloat percent = CMTimeGetSeconds(self.playerItem.currentTime) / CMTimeGetSeconds(weakSelf.playerItem.duration);
            CGFloat newX = maxX * percent;
            weakSelf.scrubberThumbView.center = CGPointMake(newX, weakSelf.scrubberThumbView.center.y);
        }];
        
        [self.player play];
        [self.playImageView setAlpha:0];
        
    } else {
        if (self.playerTimeObserver != nil) {
            [self.player removeTimeObserver:self.playerTimeObserver];
            self.playerTimeObserver = nil;
        }
        [self.player pause];
        [self.playImageView setAlpha:1];
    }
}

- (IBAction)volumeButtonPressed:(id)sender {
}


//MARK:- Scrubber Pan
- (void)scrubberThumbPanned:(UIPanGestureRecognizer *)sender {
    
    //Stop the player
    if (self.playerTimeObserver != nil) {
        [self.player removeTimeObserver:self.playerTimeObserver];
        self.playerTimeObserver = nil;
    }
    if (self.playImageView.alpha == 0) {
        [self.player pause];
        [self.playImageView setAlpha:1];
    }
    
    //Move the thumb
    if (sender.state == UIGestureRecognizerStateBegan) {
        _thumbLastLocation = self.scrubberThumbView.center;
    }
    CGPoint point = [sender translationInView:self.scrubberContainerView];
    if (point.x != self.thumbLastLocation.x) {
        CGFloat newX = _thumbLastLocation.x + point.x;
        
        //Thumb Movement
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!(newX < 0) && !(newX > self->maxXPosition)) {
                self.scrubberThumbView.center = CGPointMake(self.thumbLastLocation.x + point.x, self.scrubberThumbView.center.y);
            }
        });
        
        //Set AvPlayer Time
        CGFloat percent = newX / maxXPosition;
        CGFloat passedTime = percent * CMTimeGetSeconds(self.playerItem.duration);
        [self.playerItem seekToTime:CMTimeMakeWithSeconds(passedTime, self.playerItem.currentTime.timescale)];
    }
}

//MARK:- UICollectionView Delegate & Datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == _taskCollectionView) {
        return taskNames.count;
    } else {
        return _scrubberImages.count;
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView == _taskCollectionView) {
        return UIEdgeInsetsMake(inset, inset, inset, inset);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == _taskCollectionView) {
        return interItemSpace;
    }
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == _taskCollectionView) {
        return interItemSpace;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == _taskCollectionView) {
        return CGSizeMake(cellWidth, cellHeight);
    }
    return CGSizeMake(_scrubberImagesCollectioniew.bounds.size.width/numberOfImages, _scrubberImagesCollectioniew.bounds.size.height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == _taskCollectionView) {
        TaskCollectionVIewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"TaskCollectionVIewCell" forIndexPath: indexPath];
        
        cell.iconImageView.image = [UIImage imageNamed:taskImageNames[indexPath.item]];
        cell.taskTitleLabel.text = taskNames[indexPath.item];
        
        return cell;
    }
    
    ScrubCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ScrubCollectionViewCell" forIndexPath: indexPath];
    
    cell.iconImageView.image = [_scrubberImages objectAtIndex:indexPath.item];
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == _taskCollectionView) {
        return true;
    }
    return false;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [collectionView deselectItemAtIndexPath:indexPath animated:true];
    if (indexPath.item == 2) {
        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        FlipperViewController *flipVC = [main instantiateViewControllerWithIdentifier:@"FlipperViewController"];
        flipVC.videoAsset = self.asset;
        flipVC.delegate = self;
        [self.navigationController presentViewController:[[CommonNavigationController alloc] initWithRootViewController:flipVC] animated:true completion:nil];
    }
}
@end
