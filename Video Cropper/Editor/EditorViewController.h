//
//  EditorViewController.h
//  Video Cropper
//
//  Created by UTHABO on 14/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "BaseViewController.h"
#import <PhotosUI/PhotosUI.h>
#import "PlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface EditorViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UICollectionView *taskCollectionView;
@property (weak, nonatomic) IBOutlet PlayerView *playerView;
@property (weak, nonatomic) IBOutlet UIButton *addToMusicButton;
@property (weak, nonatomic) IBOutlet UIImageView *scrubberThumbView;
@property (weak, nonatomic) IBOutlet UIView *scrubberContainerView;
@property (weak, nonatomic) IBOutlet UICollectionView *scrubberImagesCollectioniew;
@property (nonatomic, strong) AVURLAsset* asset;
@end

NS_ASSUME_NONNULL_END
