//
//  AppDelegate.m
//  Video Cropper
//
//  Created by UTHABO on 12/4/19.
//  Copyright © 2019 Somoy. All rights reserved.
//

#import "AppDelegate.h"
#import "Utility/Constants/UIColor+AppColor.h"
#import "Utility/Base/BaseViewController.h"
#import "Utility/CommonNavigation/CommonNavigationController.h"
#import "Home/HomeViewController.h"
#import "EditorViewController.h"
#import "FlipperViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

//MARK:- AppDelegate Methods
- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    EditorViewController *editVC = [storyBoard instantiateViewControllerWithIdentifier:@"EditorViewController"];
//    FlipperViewController *flipVC = [storyBoard instantiateViewControllerWithIdentifier:@"FlipperViewController"];
    HomeViewController *base = [storyBoard instantiateViewControllerWithIdentifier: @"HomeViewController"];
    CommonNavigationController * comm = [[CommonNavigationController alloc] initWithRootViewController:base];
    
    _window.rootViewController = comm;
    [_window makeKeyWindow];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self setDefaultAppearence];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//MARK:- Set Default UIAppearence
- (void)setDefaultAppearence {
    [[UINavigationBar appearance] setBarTintColor:[UIColor NavigationBarColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor NavigationBarTint]];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys: [UIColor NavigationBarTint], NSForegroundColorAttributeName, nil]; //[UIFont systemFontOfSize:20 weight:UIFontWeightSemibold], NSFontAttributeName, nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
}

@end
